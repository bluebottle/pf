package main

import (
	"fmt"
	"os"

	"github.com/knadh/koanf"
	"github.com/urfave/cli/v2"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
	"gitlab.com/bluebottle/go-modules/technical"
	"gitlab.com/bluebottle/pf/configuration"
)

func main() {
	var (
		file string
	)
	app := &cli.App{
		Name:                   "screener",
		Usage:                  "Scanning for investable equities",
		UseShortOptionHandling: true,
		Version:                "0.2",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "file",
				Aliases:     []string{"f"},
				Usage:       "batch input file (default: screener.yml)",
				Destination: &file,
				Value:       "screener.yml",
			},
		},
		Action: func(c *cli.Context) error {
			mainWork("screener", file)

			return nil
		},
	}

	err := app.Run(os.Args)
	misc.ShowError(err, "", "ErrFatal")
} // func main

// func getEMAresults get latest values for set of EMAs and difference of price to chosen EMA value
func getEMAresults(quotes []postgresql.Quote, period string, ema int) (tEMA float64, CEs map[int]float64) {
	var (
		EMAs map[int][]float64 = make(map[int][]float64)
	)
	CEs = make(map[int]float64)

	// calculate EMAs and last EMA value
	for _, v := range []int{20, 40, 50, 100, 150, 200} {
		switch period {
		case "d":
			EMAs[v] = technical.EMA(v, postgresql.GetClose(quotes))
		case "w":
			EMAs[v] = technical.EMA(v, postgresql.GetClose(postgresql.WeeklyQuotes(quotes)))
		case "m":
			EMAs[v] = technical.EMA(v, postgresql.GetClose(postgresql.MonthlyQuotes(quotes)))
		case "y":
			EMAs[v] = technical.EMA(v, postgresql.GetClose(postgresql.YearlyQuotes(quotes)))
		} // switch

		if len(EMAs[v]) > 0 {
			CEs[v] = EMAs[v][len(EMAs[v])-1]
		} else {
			CEs[v] = -1
		} // if
	} // for

	tEMA = technical.EmaDiffPer(EMAs[ema], quotes)
	return
} // getEMAresults ()

func emaCalc(lookupVal []postgresql.Lookup, emaPeriod string, emadiff float64, ema int) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking if EMAs are stacking up and price is max %4.2f%% above EMA%d(%s) (%d securites)\n", emadiff, ema, emaPeriod, len(lookupVal))

	for _, values := range lookupVal {
		QuoteVal := postgresql.FindQuotes(values.Symbol, "asc")
		tmpEMA, CEs := getEMAresults(QuoteVal, emaPeriod, ema)

		if checkEMACond(CEs, tmpEMA, emadiff) {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// passing securites for next check
	return
} // emaCalc ()

// Keep only those securities whose EMAs are stacked in the right order and
// whose price is currently only a certain percentage above chosen EMA
func checkEMACond(CEs map[int]float64, tmpEMA, emadiff float64) (result bool) {
	return CEs[20] > CEs[40] && CEs[40] > CEs[50] && CEs[50] > CEs[100] &&
		CEs[100] > CEs[150] && CEs[150] > CEs[200] && tmpEMA <= emadiff && tmpEMA >= 0
} // checkEMACond ()

func stochCalc(lookupVal []postgresql.Lookup, stochPeriod, k, d int, daily, weekly, monthly, yearly float64) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking Stochastics(%d %d %d) are below (d%2.2f/w%2.2f/m%2.2f/y%2.2f) (%d securites)\n", stochPeriod, k, d, daily, weekly, monthly, yearly, len(lookupVal))
	var evalValues map[string]technical.Evaluate

	for _, values := range lookupVal {
		evalValues = technical.CompleteCurrentStochastic(values.Symbol, stochPeriod, k, d, daily, weekly, monthly, yearly)

		if evalValues["D"].IsLower && evalValues["W"].IsLower &&
			(evalValues["M"].IsLower || evalValues["M"].CurrentValue == -1.0) &&
			(evalValues["Y"].IsLower || evalValues["Y"].CurrentValue == -1.0) {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// keep only passing securites for next check
	return
} // stochCalc()

func willCalc(lookupVal []postgresql.Lookup, willPeriod int, wdaily, wweekly, wmonthly, wyearly float64) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking Williams%%R(%d) is below (d%2.2f/w%2.2f/m%2.2f/y%2.2f) (%d securites)\n", willPeriod, wdaily, wweekly, wmonthly, wyearly, len(lookupVal))
	var evalValues map[string]technical.Evaluate

	for _, values := range lookupVal {
		evalValues = technical.CompleteCurrentWilliamsR(values.Symbol, willPeriod, wdaily, wweekly, wmonthly, wyearly)

		if evalValues["D"].IsLower && evalValues["W"].IsLower &&
			(evalValues["M"].IsLower || evalValues["M"].CurrentValue == -1.0) &&
			(evalValues["Y"].IsLower || evalValues["Y"].CurrentValue == -1.0) {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// keep only passing securites for next check
	return
} // willCalc ()

func macdCalc(lookupVal []postgresql.Lookup, emaPeriod string, emaLow, emaHigh, emaSignal int) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking if MACD(%s,%d,%d,%d) is positive (%d securites)\n", emaPeriod, emaLow, emaHigh, emaSignal, len(lookupVal))

	//
	// TODO: Add check for just switch positive?
	//
	for _, values := range lookupVal {
		ma, signal := technical.MACD(lookupVal, values.Symbol, emaPeriod, emaLow, emaHigh, emaSignal)

		// Keep only those securities whose MACD is positive
		if (ma[len(ma)-1] - signal[len(signal)-1]) > 0 {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// passing securites for next check
	return
} // macdCalc ()

func mainLoop(lookupVal []postgresql.Lookup, batch *koanf.Koanf) (retLook []postgresql.Lookup) {
	// Initialize array of lookup values
	retLook = lookupVal

	for _, k := range batch.Slices("tasks") {
		switch {
		case k.Exists("EMA"):
			retLook = emaCalc(
				retLook,
				k.String("EMA.emaPeriod"),
				k.Float64("EMA.emadiff"),
				k.Int("EMA.ema"),
			)
		case k.Exists("MACD"):
			retLook = macdCalc(
				retLook,
				k.String("MACD.emaPeriod"),
				k.Int("MACD.emaLow"),
				k.Int("MACD.emaHigh"),
				k.Int("MACD.emaSignal"),
			)
		case k.Exists("Stochastic"):
			retLook = stochCalc(
				retLook,
				k.Int("Stochastic.stochPeriod"),
				k.Int("Stochastic.stochK"),
				k.Int("Stochastic.stochD"),
				k.Float64("Stochastic.daily"),
				k.Float64("Stochastic.weekly"),
				k.Float64("Stochastic.monthly"),
				k.Float64("Stochastic.yearly"),
			)
		case k.Exists("Williams%R"):
			retLook = willCalc(
				retLook,
				k.Int("Williams%R.willPeriod"),
				k.Float64("Williams%R.wdaily"),
				k.Float64("Williams%R.wweekly"),
				k.Float64("Williams%R.wmonthly"),
				k.Float64("Williams%R.wyearly"),
			)
		default:
			fmt.Println("Found wrong element")
		} // switch
	} // for

	return
} // mainLoop

// Scans all equities depending on the checks and values chosen
func mainWork(appname string, filename string) {
	// Read config files
	//
	// TODO: Minimize the use of hardcoded values
	//
	config := configuration.ReadProgConfig("/etc/chartit/chartit.yml")
	batch := configuration.ReadBatchConfig(filename)
	// open database connection
	err := postgresql.NewDBConnection(
		config.String("database.user"),
		config.String("database.host"),
		config.String("database.password"),
		config.String("database.port"),
		config.String("database.dbname"),
		appname,
		config.String("database.sslmode"))
	misc.ShowError(err, "Could not open postgres database.", "ErrMsgFatal")

	var (
		lookupVal []postgresql.Lookup
	)

	// Get lookup values if any tasks are given
	if batch.Exists("tasks") && batch.MustString("tasks") != "" {
		lookupVal = postgresql.FindAllCurrentLookup()
		// Print header
		fmt.Printf("Scanning %d securities\n", len(lookupVal))
	} else {
		fmt.Println("No tasks given")
	} // if

	// First run through the loop
	lookupVal = mainLoop(lookupVal, batch)
	// output passing securities
	fmt.Printf("%d securities passed screening\n", len(lookupVal))
	fmt.Println("Updating resulting values")
	// Update all resulting values, to make sure we are scanning the latest values
	postgresql.UpdateQuoteLoop(lookupVal)
	// Final run through the loop
	lookupVal = mainLoop(lookupVal, batch)
	// output passing securities
	fmt.Printf("%d securities passed screening\n", len(lookupVal))

	if len(lookupVal) > 0 {
		fmt.Println("\nName (ISIN/Symbol)")

		for _, v := range lookupVal {
			fmt.Printf("%s (%s/%s)\n", v.Name, v.ISIN, v.Symbol)
		} // for
	} // if

	postgresql.CloseConnection()
} // mainWork ()
