\c pf
insert into public.portfolio ("name", trade_risk, port_risk) values ('test1', 1.0, 10.0);
insert into public.currencies (currency, currencycountry, iso_code, symbol) values ('Euro', 'European Union', 'EUR', '€');
insert into public.currencies (currency, currencycountry, iso_code, symbol) values ('US Dollar', 'United States', 'USD', '$');
insert into public.currencies (currency, currencycountry, iso_code, symbol) values ('Swizz Franc', 'Switzerland', 'CHF', 'CHF');
insert into public.currencies (currency, currencycountry, iso_code, symbol) values ('Australia Dollar', 'Australia', 'AUD', '$');
insert into public.currencies (currency, currencycountry, iso_code, symbol) values ('Canada Dollar', 'Canada', 'CAD', '$');
insert into public.currencies (currency, currencycountry, iso_code, symbol) values ('United Kingdom Pound', 'UK', 'GBP', '£');
insert into public.currencies (currency, currencycountry, iso_code, symbol) values ('Hong Kong Dollar', 'Hong Kong', 'HKD', '$');
insert into public.lookup (symbol, "name", wkn, isin, sector, look_type, quote_currency, is_history) values ('ariva::2331.16', 'Entegris', '938201', 'US29362U1043','None', 'Stock', 'EUR', false);
