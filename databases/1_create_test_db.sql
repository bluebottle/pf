DROP DATABASE IF EXISTS pf;

CREATE DATABASE pf ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8' TABLESPACE = default OWNER = postgres TEMPLATE template0;
\connect pf

-- Contains all the currencies used by the symbols
CREATE TABLE public.currencies (
	currency varchar(255) NOT NULL,
	currencycountry varchar(255) NOT NULL,
	iso_code varchar(3) NOT NULL,
	symbol varchar(3) NOT NULL
);
CREATE UNIQUE INDEX currencies_name_idx ON public.currencies USING btree (iso_code);

ALTER TABLE public.currencies OWNER TO postgres;

-- Contains the portfolios
CREATE TABLE IF NOT EXISTS public.portfolio (
  portfolio_id       int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
  "name"             character varying(40) COLLATE pg_catalog."default" NOT NULL,
  trade_risk         numeric(5,2) NOT NULL DEFAULT 1.0,
  port_risk          numeric(5,2) NOT NULL DEFAULT 10.0,
  CONSTRAINT portfolio_pk PRIMARY KEY (portfolio_id)
);

ALTER TABLE public.portfolio OWNER TO postgres;


-- Contains the depositions and withdrawals for each portfolio
CREATE TYPE flow_direction AS ENUM ('Deposit', 'Withdrawal');

CREATE TABLE IF NOT EXISTS public.portfolio_moneyflow (
	portfolio_id      int8 NOT NULL DEFAULT 0,
	flow_date         timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
	port_direction    flow_direction NOT NULL DEFAULT 'Deposit',
	flow_amount       numeric(30,10) NOT NULL DEFAULT 0.0,
	CONSTRAINT moneyflow_pkey PRIMARY KEY (portfolio_id),
	CONSTRAINT fk_trades_portfolio FOREIGN KEY (portfolio_id) REFERENCES public.portfolio(portfolio_id) ON DELETE CASCADE ON UPDATE CASCADE
);

ALTER TABLE public.portfolio_moneyflow OWNER TO postgres;

-- Contains the information for all symbols
CREATE TYPE types AS ENUM ('Currency', 'Stock', 'Bond Fund', 'Ethical Fund', 'Funds of funds', 'Index Fund', 'International Fund', 'Money Market Fund', 'Regional Fund', 'Sector Fund', 'Index', 'Sector', 'Warrant', 'ETF', 'Managed Fund', 'Commodity', 'Testindex', 'Discount Certificate', 'Turbo Certificate', 'Other');
CREATE TYPE cap AS ENUM ('Micro', 'Small', 'Mid', 'Large');

CREATE TABLE IF NOT EXISTS public.lookup (
  symbol             character varying(50) UNIQUE COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  name               character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  wkn                character varying(10) COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  isin               character varying(30) COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  sector             character varying(255) COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  look_type          types NOT NULL DEFAULT 'Stock',
  quote_currency     character varying(10) COLLATE pg_catalog."default" NOT NULL DEFAULT 'EUR',
  is_history         boolean NOT NULL DEFAULT false,
  CONSTRAINT lookup_pkey PRIMARY KEY (symbol),
  CONSTRAINT lookup_curr FOREIGN KEY (quote_currency) REFERENCES public.currencies(iso_code) ON DELETE CASCADE ON UPDATE CASCADE
);

ALTER TABLE public.lookup OWNER TO postgres;
CREATE UNIQUE INDEX i_lookup_symbol ON public.lookup (symbol);
CREATE INDEX i_lookup_type ON public.lookup (look_type);
CREATE INDEX i_lookup_wkn ON public.lookup (wkn);
CREATE UNIQUE INDEX i_lookup_isin ON public.lookup (isin);
CREATE INDEX i_lookup_history ON public.lookup (is_history);

-- Contains all the quotes for the symbols
CREATE TABLE IF NOT EXISTS public.quote (
  symbol             character varying(50) COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  quote_date         timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  open               numeric(10,3) NOT NULL DEFAULT 0.0,
  high               numeric(10,3) NOT NULL DEFAULT 0.0,
  low                numeric(10,3) NOT NULL DEFAULT 0.0,
  close              numeric(10,3) NOT NULL DEFAULT 0.0,
  volume             bigint NOT NULL DEFAULT 0,
  CONSTRAINT quote_pkey            PRIMARY KEY (symbol,quote_date),
  CONSTRAINT quote_open_check      CHECK (open   >= 0.0),
  CONSTRAINT quote_high_check      CHECK (high   >= 0.0),
  CONSTRAINT quote_highlow_check   CHECK (high   >= low),
  CONSTRAINT quote_highopen_check  CHECK (high   >= open),
  CONSTRAINT quote_highclose_check CHECK (high   >= close),
  CONSTRAINT quote_low_check       CHECK (low    >= 0.0),
  CONSTRAINT quote_lowhigh_check   CHECK (low    <= high),
  CONSTRAINT quote_lowopen_check   CHECK (low    <= open),
  CONSTRAINT quote_lowclose_check  CHECK (low    <= close),
  CONSTRAINT quote_close_check     CHECK (close  >= 0.0),
  CONSTRAINT quote_volume_check    CHECK (volume >= 0)
);

ALTER TABLE public.quote OWNER TO postgres;

ALTER TABLE public.quote
    ADD CONSTRAINT fk_quote_symbol FOREIGN KEY (symbol)
    REFERENCES public.lookup (symbol) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

-- Contains all the trades for the symbols
CREATE TYPE stopLossType AS ENUM ('%', 'Fixed', 'None');

CREATE TABLE IF NOT EXISTS public.trades (
	trade_id          bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
	portfolio_id      int8 NOT NULL DEFAULT 0,
	symbol            varchar(50) NOT NULL DEFAULT ''::character varying,
	"comment"         varchar(255) NULL DEFAULT ''::character varying,
	check_level_value numeric(10, 3) NOT NULL DEFAULT 0.0,
	stop_loss_type    stoplosstype NOT NULL DEFAULT 'None'::stoplosstype,
	stop_loss_value   numeric(10, 3) NOT NULL DEFAULT 0.0,
	CONSTRAINT trades_un           UNIQUE (trade_id),
	CONSTRAINT fk_trades_portfolio FOREIGN KEY (portfolio_id) REFERENCES public.portfolio(portfolio_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_trades_symbol    FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX id_portfolio ON public.trades USING btree (portfolio_id);

ALTER TABLE public.trades OWNER TO postgres;

-- Contains all the transactions for the symbols
CREATE TYPE actions AS ENUM ('Buy', 'Dividend', 'Sale', 'Split', 'Swap');

CREATE TABLE IF NOT EXISTS public.transactions (
	trans_id     int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	trade_id     int8 NOT NULL,
	portfolio_id int8 NOT NULL DEFAULT 0,
	symbol       varchar(50) NOT NULL DEFAULT ''::character varying,
	"date"       timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
	"action"     actions NOT NULL DEFAULT 'Buy'::actions,
	price        numeric(10, 3) NOT NULL DEFAULT 0.0,
	quantity     numeric(10, 3) NOT NULL DEFAULT 0.0,
	brokerage    numeric(10, 3) NOT NULL DEFAULT 0.0,
	slippage     numeric(10, 3) NOT NULL DEFAULT 0.0,
	CONSTRAINT transactions_un        UNIQUE (trans_id),
	CONSTRAINT transactions_lookup    FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT transactions_portfolio FOREIGN KEY (portfolio_id) REFERENCES public.portfolio(portfolio_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT transactions_trades    FOREIGN KEY (trade_id) REFERENCES public.trades(trade_id) ON DELETE CASCADE ON UPDATE CASCADE
);

ALTER TABLE public.transactions OWNER TO postgres;

-- Contains all the warrants for the symbols
CREATE TABLE public.warrant (
	symbol           varchar(50) NOT NULL,
	base_symbol      varchar(50) NOT NULL,
	warrant_end_date date NOT NULL,
	CONSTRAINT warrant_base_lookup FOREIGN KEY (base_symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT warrant_lookup      FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX warrant_symbol_idx ON public.warrant USING btree (symbol);

ALTER TABLE public.warrant OWNER TO postgres;

-- Contains all the watchlists
CREATE TABLE public.watchlists (
	watchlist_id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	watch_name varchar(255) NOT NULL
);
CREATE UNIQUE INDEX watchlists_name ON public.watchlists USING btree (watch_name);
CREATE UNIQUE INDEX watchlists_id ON public.watchlists USING btree (watchlist_id);

ALTER TABLE public.watchlists OWNER TO postgres;

-- Contains all the watchlist content
CREATE TABLE public.watchlist_content (
	watchlist_id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	symbol           varchar(50) NOT NULL,
	CONSTRAINT watchlist_content_symbol FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT watchlist_content_id	FOREIGN KEY (watchlist_id) REFERENCES public.watchlists(watchlist_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX watchlist_content_idx ON public.watchlist_content USING btree (watchlist_id);

ALTER TABLE public.watchlist_content OWNER TO postgres;
