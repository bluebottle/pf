package main

import (
	"fmt"
	"html/template"
	"time"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
)

// Contains the values of one transaction inside the portfolio
type portTransValues struct {
	avgBuyPrice   float64
	buyDate       time.Time
	quantity      float64
	checkValue    float64
	stopLossValue float64
	StopLossType  string
	BuyBrokerage  float64
	BuySlippage   float64
}

func portfolio(portfolio string) (content template.HTML) {
	portValues, err := postgresql.FindSpecificPortfolio(portfolio)

	if err != nil {
		content = template.HTML(template.HTMLEscapeString("Could no read portfolio values of " + portfolio + ` Error: ` + err.Error()))
	} else {
		//
		// TODO: Add current capital by calculating (entered capital) - (spent capital)
		// TODO: Add functionality to add/delete positions
		// TODO: Add position add window (Position added via symbols window??)
		//
		content = template.HTML(
			`<div id="portfolio_content">
			<div id="` + portfolio + `_portfolio_side">
				<input id="` + portfolio + `_toggle" type=checkbox>
				<label for="` + portfolio + `_toggle">Overview</label>
				<div id="` + portfolio + `_expand">
					<p>ID:` + fmt.Sprintf("%d", portValues.PortfolioID) + `</p>
					<p>Risk per trade:` + fmt.Sprintf("%f", portValues.TradeRisk) + `</p>
					<p>Risk per portfolio:` + fmt.Sprintf("%f", portValues.PortRisk) + `</p>
				</div>
			</div>
			<div id="` + portfolio + `_portfolio_content_right">
			`)

		tradeValues, err := postgresql.FindPortfolioTrades(portValues.PortfolioID)

		if err != nil {
			content += template.HTML("Could no read trade values for portfolio " + portfolio + ` Error: ` + err.Error() + `</div>`)
		} else {
			content += template.HTML(`<table>
				<tr>
					<th colspan="7">Buy</th>
					<th>Current</th>
				</tr>
				<tr>
					<th>Symbol</th>
					<th>Name</th>
					<th>Quantity</th>
					<th>Price</th>
					<th>Date</th>
					<th>Brokerage</th>
					<th>Slippage</th>
					<th>Price</th>
				</tr>`)

			// range over all trade results
			for k := range tradeValues {
				// get name of current symbol
				tmpName := postgresql.GetSingleLookup(tradeValues[k].Symbol)
				tmpQuote := postgresql.GetLastQuote(tradeValues[k].Symbol)
				tmpTransactions, err := postgresql.FindPortTradeTransactions(tradeValues[k].PortfolioID, tradeValues[k].TradeID)
				misc.ShowError(err, "", "ErrPrint")
				tmpBuyPrice := calculatePortValues(tmpTransactions)
				tmpBuyPrice.calcStopLoss(&tradeValues[k], tmpQuote.Close)
				// add row to output
				content += template.HTML(`<tr>
					<td>` + tradeValues[k].Symbol + `</td>
					<td>` + tmpName.Name + `</td>
					<td>` + misc.FormatFloat(tmpBuyPrice.quantity, 2, false) + `</td>
					<td>` + misc.FormatFloat(tmpBuyPrice.avgBuyPrice, 2, false) + " " + postgresql.GetCurrencySymbol(tmpName.QuoteCurrency).Symbol + `</td>
					<td>` + tmpBuyPrice.buyDate.Format("02-01-2006") + `</td>
					<td>` + misc.FormatFloat(tmpBuyPrice.BuyBrokerage, 2, false) + `</td>
					<td>` + misc.FormatFloat(tmpBuyPrice.BuySlippage, 2, false) + `</td>
					<td ` + stopColor(tmpQuote.Close, tmpBuyPrice.stopLossValue, tmpBuyPrice.checkValue) + ` title="Stoploss type: ` + tmpBuyPrice.StopLossType + `
 Stoploss value: ` + misc.FormatFloat(tmpBuyPrice.stopLossValue, 2, false) + " " + postgresql.GetCurrencySymbol(tmpName.QuoteCurrency).Symbol + `">` +
					misc.FormatFloat(tmpQuote.Close, 2, false) + " " + postgresql.GetCurrencySymbol(tmpName.QuoteCurrency).Symbol + `</td>
					</tr>`)
			} // for

			content += template.HTML(`</table>
			</div>
		</div>`)
		} // if
	} // if

	//
	// TODO: add config to modify some values
	//
	return
} // portfolio()

// check difference between current price and stop loss and set background color
func stopColor(curr, stop, check float64) (backClass string) {
	switch {
	case curr < check:
		backClass = "class=yellow"
	case stop > curr:
		backClass = "class=red"
	default:
		backClass = "class=green"
	} // switch

	return
} // stopColor ()

// calculatePortValues calculates the average buy price, total brokerage and slippage
func calculatePortValues(transactions []postgresql.Transaction) (portValue portTransValues) {
	portValue.avgBuyPrice = 0.0
	portValue.quantity = 0.0
	portValue.buyDate = time.Now()
	portValue.BuyBrokerage = 0.0
	portValue.BuySlippage = 0.0

	for _, v := range transactions {
		if v.TransDate.Before(portValue.buyDate) {
			portValue.buyDate = v.TransDate
		} // if

		portValue.avgBuyPrice += v.Price * v.Quantity
		portValue.quantity += v.Quantity
		portValue.BuyBrokerage += v.Brokerage
		portValue.BuySlippage += v.Slippage
	} // for

	portValue.avgBuyPrice /= portValue.quantity
	return
} // calculatePortValues()

// calcStopLossValue calutates stop loss value of trade
func (p *portTransValues) calcStopLoss(trade *postgresql.Trade, currClose float64) {
	switch trade.StopLossType {
	case "Fixed":
		p.stopLossValue = trade.StopLossValue
		p.StopLossType = trade.StopLossType
	case "%":
		p.stopLossValue = currClose * (1 - (trade.StopLossValue / 100.0))
		p.StopLossType = fmt.Sprintf("%3.2f %%", trade.StopLossValue)
	default:
		p.stopLossValue = 0.0
		p.StopLossType = trade.StopLossType
	} // switch

	p.checkValue = trade.CheckLevelValue
} // calcStopLossValue()

func portaddwin() (content template.HTML) {
	//
	// TODO: Add functionality to add a new portfolio (form, checks, buttons, status messages)
	// TODO: store all the values
	//
	content = template.HTML(`<div id="addportform">
		<label for="paddname">Name:</label>
		<input name="paddname" id="paddname" type="text" size="50" maxlength="255" value="" />
		<label for="paddtrade_risk">Risk per trade:</label>
		<input name="paddtrade_risk" id="paddtrade_risk" type="text" size="30" maxlength="30" value="" />
		<label for="paddport_risk">Risk per portfolio:</label>
		<input name="paddport_risk" id="paddport_risk" type="text" size="30" maxlength="255" value="" />
	</div>
	<div id="addportbuttons">
		<input type="button" onclick="portaddfunc()" value="Save">
	</div>`)
	return
} // portaddwin()

func portposaddwin(symbol string) (content template.HTML) {
	tmpLookup := postgresql.GetSingleLookup(symbol)
	currPortfolios := getPortfolios()

	content = template.HTML(`<div id="portposaddform">
	<p><label for="ppaddport_portchoice">Portfolio:</label></p><p><select name="ppaddport_portchoice" id="ppaddport_portchoice" size="1">`)

	for _, v := range currPortfolios {
		content += template.HTML(`<option>` + v + `</option>`)
	} // for

	content += template.HTML(`</select></p>
	<p>Name:</p><p>` + tmpLookup.Name + `</p>
	<p><label for="ppaddport_quantity">Quantity:</label></p><p><input name="ppaddport_quantity" id="ppaddport_quantity" type="text" size="5" maxlength="10" value="" /></p>
	<p><label for="ppaddport_price">Buy price:</label></p><p><input name="ppaddport_price" id="ppaddport_price" type="text" size="5" maxlength="10" value="" /></p>
	<p><label for="ppaddport_brokerage">Brokerage:</label></p><p><input name="ppaddport_brokerage" id="ppaddport_brokerage" type="text" size="5" maxlength="10" value="" /></p>
	<p><label for="ppaddport_slippage">Slippage:</label></p><p><input name="ppaddport_slippage" id="ppaddport_slippage" type="text" size="5" maxlength="10" value="" /></p>
</div>
<div id="portposaddbuttons">
	<input type="button" onclick="portposaddfunc()" value="Add">
</div>`)

	//
	// TODO: Add fields to add new position
	//
	return
} // portposaddwin()
