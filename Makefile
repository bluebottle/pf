# Go parameters
GOFMT      := gofmt
GOCMD      := go
GOBUILD    := GO111MODULE=on $(GOCMD) build -v
#GOBUILD    := GO111MODULE=on $(GOCMD) build -v -mod=vendor
GOCLEAN    := $(GOCMD) clean
GOINSTALL  := GO111MODULE=on $(GOCMD) install
GOTEST     := GO111MODULE=on $(GOCMD) test -v
GOVET      := GO111MODULE=on $(GOCMD) vet
GORACE     := $(GOCMD) race
GOTOOL     := GO111MODULE=on $(GOCMD) tool
GOLINT     := golint

SRCS       := $(shell find . -name "*.go" -type f ! -path "./vendor/*" ! -path "*/bindata.go")

export GO111MODULE=off

PACKAGES ?= $(shell find . -name "*.go" -print|grep -v /vendor/)

ifndef VERBOSE
.SILENT:
endif

.DEFAULT_GOAL := help

all: dep fmt test build coverage  ## Run all

dep: ## Get all the dependencies
		@echo "Getting dependencies"
		$(GOINSTALL) ./...
		$(GOINSTALL) github.com/fzipp/gocyclo/cmd/gocyclo@latest
		$(GOINSTALL) github.com/uudashr/gocognit/cmd/gocognit@latest
		go get -u github.com/gordonklaus/ineffassign
		$(GOINSTALL) github.com/kisielk/errcheck@latest
		$(GOINSTALL) github.com/securego/gosec/v2/cmd/gosec@latest
		$(GOINSTALL) honnef.co/go/tools/cmd/staticcheck@latest
		$(GOINSTALL) github.com/go-critic/go-critic/cmd/gocritic@latest

fmt: ## Format source code
		@echo "Formatting code"
		GO111MODULE=on $(GOFMT) -w -s $(PACKAGES)

test: ## Running tests for files
		@echo "Running tests"
		@echo "============="
		@echo "Running staticcheck"
		GO111MODULE=on staticcheck -checks all
		@echo "Running gocyclo"
		# ignore error code, we just want the messages
		-gocyclo -over 15 -ignore vendor .
		@echo "Running gocognit"
		gocognit -over 15 .|grep -v vendor
		@echo "Running ineffassign"
		GO111MODULE=on ineffassign ./...
		@echo "Running errcheck"
		GO111MODULE=on errcheck -asserts -blank -verbose ./...
		@echo "Running gosec"
		GO111MODULE=on gosec -no-fail -exclude=G203 -tests ./...
		@echo "Running gocritic"
		GO111MODULE=on gocritic check -enableAll -disable='commentedOutCode'
		@echo "Running $(GOVET)"
		$(GOVET) ./...
		@echo "Running $(GOTEST) race condition"
		$(GOTEST) -race ./...
		#@echo "Running $(GOTEST) race memory sanity"
		#CC=clang $(GOTEST) -msan ./.
		@echo "Running tests"
		$(GOTEST) ./. || exit 1;
		@echo "Running benchs"
		$(GOTEST) -bench ./.
		@echo ""

build: ## Building binary
		@echo "Running $(GOBUILD)"
		@echo "================"
		$(GOBUILD) -o pf main.go win.go portfolio.go symbols.go watchlists.go;
		$(GOBUILD) -o batch/updateloop/updateloop batch/updateloop/main.go
		$(GOBUILD) -o batch/screener/screener batch/screener/main.go
		@echo ""

coverage: ## Generating coverage for files
		@echo "Running coverage"
		@echo "================"
		$(GOTEST) ./... -coverpkg=./... -coverprofile coverage.cov
		$(GOTOOL) cover -func=coverage.cov

clean: ./* ## Cleaning up
		@echo "Cleaning package"
		@echo "================"
		rm -f *.so
		rm -f *.cov
		rm -f pf
		rm -f batch/updateloop/updateloop
		rm -f batch/screener/screener

help:  ## Displaying help for build targets
		@echo "Available targets in this makefile:"
		@echo ""
		@grep -E '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
