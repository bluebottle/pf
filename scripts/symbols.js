//
// TODO: Handle output (error, success,...)
//
function symboladdfunc() {
    fetch ('/work?type=sadd&saddsymbol='+document.getElementById("saddsymbol").value+
        '&saddname='+document.getElementById("saddname").value+
        '&saddwkn='+document.getElementById("saddwkn").value+
        '&saddisin='+document.getElementById("saddisin").value+
        '&saddsector='+document.getElementById("saddsector").value+
        '&saddtype='+document.getElementById("saddtype").value+
        '&saddquote_cur='+document.getElementById("saddquote_cur").value+
        '&saddbasesym='+document.getElementById("saddbasesym").value+
        '&saddoptionend='+document.getElementById("saddoptionend").value)
    .then(response => response.text())
    .then(data => {
    })
    .catch(err => console.log(err))
};

function warrantToggle () {
    var addType = document.getElementById("saddtype")
    var warrExtra = document.getElementById("warrantextras")

    if (addType.value === "Warrant") {
        warrExtra.style.display = "block";
    } else {
        warrExtra.style.display = "none";
    }
}

function symbolToggle() {
  // get data to update watchlist menu
  fetch ("work?type=template&element=symboltable&value="+document.getElementById("symFilterValues").value+"&col=symbol&dir=asc")
  .then(response => response.text())
  .then(data => { document.getElementById("symbolscontent").innerHTML = data })
  .catch(err => console.log(err))
};

function sortme(element) {
    fetch ("work?type=template&element=symboltable&value="+document.getElementById("symFilterValues").value+"&col="+element.srcElement.parentElement.id+"&dir="+element.srcElement.classList.value)
    .then(response => response.text())
    .then(data => { document.getElementById("symbolscontent").innerHTML = data })
    .catch(err => console.log(err))
}