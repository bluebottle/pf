// This script handles the creation and deletion of draggable windows

// win opens a new draggable window if the id does not already exist
function win(event, id, type) {
    if (document.getElementById(type+id) == null) {
      fetch ('/work?type='+type+'&element='+id)
      .then(response => response.text())
      .then(data => {
        var e = document.createElement("template");
        var r = document.createRange();
        r.selectNodeContents(e);
        var s = data
        var f = r.createContextualFragment(s);
        e.appendChild(f);
        e = e.firstElementChild;
        document.getElementById("desktop").appendChild(e);
        dragElement(document.getElementById(type+id));
    });
  } else {
    console.log ("window with id: " + type + id + " exists")
  }
}

// finish closes the parent window
function finish(event) {
  const element = document.getElementById(event.target.parentNode.id);
  element.remove();
}