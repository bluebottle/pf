package main

import (
	"bytes"
	"html/template"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
)

func symbols() (content template.HTML) {
	//
	// TODO: Add extra actions (delete, alter, evaluate,... symbol)
	//
	t, err := template.New("symboltable.tmpl").Delims("{[{", "}]}").Funcs(funcMap).ParseGlob("./templates/*.tmpl")
	misc.ShowError(err, "", "ErrFatal")
	t, err = t.Parse("{[{ template \"symboltable\" . }]}")
	misc.ShowError(err, "", "ErrFatal")

	hp := struct {
		Value  []postgresql.Lookup
		Option string
	}{Value: getSymbols("all", "symbol", "asc"), Option: "all"}

	var tpl bytes.Buffer
	err = t.Execute(&tpl, &hp)
	misc.ShowError(err, "", "ErrFatal")
	content = template.HTML(tpl.String())
	return
} // intrinsic()

// addTypeTitle returns a HTML title if type is a warrant, otherwise an empty string is returned
func addTypeTitle(looktype, symbol string) (title template.HTMLAttr) {
	title = ""

	if looktype == "Warrant" {
		tmpWar, err := postgresql.FindSpecificWarrant(symbol)

		if err == nil && tmpWar.Base_symbol != "" {
			tmpLook := postgresql.GetSingleLookup(tmpWar.Base_symbol)
			title = template.HTMLAttr(" title=\"Base symbol: " + tmpLook.Name + " (" + tmpLook.ISIN + ")\"")
		} else {
			misc.ShowError(err, "Could not find basesymbol for "+symbol, "ErrFatal")
		} // if
	} // if

	return
} // addTypeTitle()

// symaddwin creates the form to add a new synbol into the system
func symaddwin() (content template.HTML) {
	//
	// TODO: Add functionality to add a new symbol (form, checks, buttons, status messages)
	// TODO: Handle extra fields (warrant end date, base symbol, iso_code, ...), depending on chosen lookup type
	// TODO: store all the values, depending on chosen lookup type
	// TODO: Choose base:symbol from existing ones and/or add new one?
	// TODO: Add checks for fields (on save or while typing?)
	//
	content = template.HTML(`<div id="addsymform">
		<label for="saddsymbol">Symbol:</label>
		<input name="saddsymbol" id="saddsymbol" type="text" size="20" maxlength="20" value="" />
		<label for="saddname">Name:</label>
		<input name="saddname" id="saddname" type="text" size="50" maxlength="255" value="" />
		<label for="saddwkn">WKN:</label>
		<input name="saddwkn" id="saddwkn" type="text" size="10" maxlength="10" value="" />
		<label for="saddisin">Isin:</label>
		<input name="saddisin" id="saddisin" type="text" size="30" maxlength="30" value="" />
		<label for="saddsector">Sector:</label>
		<input name="saddsector" id="saddsector" type="text" size="30" maxlength="255" value="" />
		<label for="saddtype">Type:</label>
		<select name="saddtype" id="saddtype" size="1" onChange="warrantToggle()">
			<option>Currency</option>
			<option selected="selected">Stock</option>
			<option>Bond Fund</option>
			<option>Ethical Fund</option>
			<option>Funds of funds</option>
			<option>Index Fund</option>
			<option>International Fund</option>
			<option>Money Market Fund</option>
			<option>Regional Fund</option>
			<option>Sector Fund</option>
			<option>Index</option>
			<option>Sector</option>
			<option>Warrant</option>
			<option>ETF</option>
			<option>Managed Fund</option>
			<option>Commodity</option>
			<option>Testindex</option>
			<option>Discount Certificate</option>
			<option>Turbo Certificate</option>
			<option>Other</option>
		</select>
		<label for="saddquote_cur">Quote currency:</label>
		<select name="saddquote_cur" id="saddquote_cur" size="1">`)
	// get all available currencies
	tmpAllCur := postgresql.GetAllCurrencies()

	// range over all symbol results
	for _, v := range tmpAllCur {
		content += template.HTML(`<option>` + v.Iso_code + `</option>`)
	} // for

	content += template.HTML(`</select>
		&nbsp;
		<div id="warrantextras">
			<label for="saddbasesym">Base symbol:</label>
			<input name="saddbasesym" id="saddbasesym" type="text" size="10" maxlength="20" value="" />
			<label for="saddoptionend">Warrant end date:</label>
			<input name="saddoptionend" id="saddoptionend" type="text" size="10" maxlength="20" value="" />
		</div>
	</div>
	<div id="addsymbuttons">
		<input type="button" onclick="symboladdfunc()" value="Save">
	</div>`)
	return
} // symaddwin()
