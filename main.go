package main

import (
	"bytes"
	"html/template"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"

	"github.com/gin-gonic/gin"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
	"gitlab.com/bluebottle/pf/configuration"
)

const version = "0.0.1"

// set function map for templates
var funcMap = template.FuncMap{
	"getportfolios": getPortfolios,
	"getwatchlists": getWatchlists,
	"getsymbols":    getSymbols,
	"addTitle":      addTypeTitle,
}

// Reads all available portfolios from the database
func getPortfolios() (output []string) {
	tmpPortfolios, err := postgresql.FindAllPortfolios()
	misc.ShowError(err, "", "ErrFatal")

	for _, v := range tmpPortfolios {
		output = append(output, v.Name)
	} // for

	return
} // getPortfolios()

// Reads all available watchlists from the database
func getWatchlists() (output []string) {
	tmpWatchlist, err := postgresql.FindAllWatchlists()
	misc.ShowError(err, "", "ErrPanic")

	for _, v := range tmpWatchlist {
		output = append(output, v.WatchlistName)
	} // for

	return
} // getWatchlists()

// Reads all available symbols from the database
func getSymbols(input, column, direction string) (output []postgresql.Lookup) {
	switch input {
	case "historic":
		output = postgresql.FindAllHistoricLookupOrdered(column, direction)
	case "current":
		output = postgresql.FindAllCurrentLookupOrdered(column, direction)
	default:
		output = postgresql.FindAllLookupOrdered(column, direction)
	} // switch

	return
} // getSymbols()

// TODO simplify (gocognit 21)
// TODO add comment
func setupRouter() (router *gin.Engine) {
	router = gin.Default()
	err := router.SetTrustedProxies(nil)
	misc.ShowError(err, "", "ErrFatal")
	// Set delimiters used in templates
	router.Delims("{[{", "}]}")
	router.SetFuncMap(funcMap)
	router.LoadHTMLGlob("./templates/*.tmpl")
	router.Static("/css", "./css")
	router.Static("/scripts", "./scripts")
	router.StaticFile("/favicon.png", "./resources/favicon.png")

	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"title":      "Main website",
			"watchlists": getWatchlists(),
		})
	})
	// Catchall redirect
	router.GET("/:action", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/")
	})

	// Route for actions that require redirection, like creating new portfolios
	router.GET("/work", func(c *gin.Context) {
		tmpTyp := c.Query("type")

		switch tmpTyp {
		case "portfolio", "symbols", "about", "help", "intrinsic", "watchlist", "symaddwin", "portaddwin", "watchaddwin", "portposaddwin":
			// get element to use for object/template creation and create window
			createDragWindow(c.Query("element"), c, tmpTyp)
		case "wadd", "padd", "sadd":
			handleInput(c, tmpTyp)
		case "template":
			element := c.Query("element")

			if element != "" {
				t, err := template.New(element+".tpml").Delims("{[{", "}]}").Funcs(funcMap).ParseGlob("./templates/*.tmpl")

				if err == nil {
					//
					// TODO: Fix error when non-existing template is used
					//
					t, err = t.Parse("{[{ template \"" + element + "\" . }]}")

					if err != nil {
						misc.ShowError(err, "", "ErrPrint")
					} else {
						var tpl bytes.Buffer

						if element == "symboltable" {
							hp := struct {
								Value  []postgresql.Lookup
								Option string
							}{Value: getSymbols(c.Query("value"), c.Query("col"), c.Query("dir")), Option: c.Query("value")}

							err = t.Execute(&tpl, &hp)
						} else {
							err = t.Execute(&tpl, nil)
						} // if

						misc.ShowError(err, "", "ErrFatal")
						c.Data(http.StatusOK, "text/plain", tpl.Bytes())
					} // if
				} // if
			} // if
		default:
			log.Info(`Given type "` + tmpTyp + `" not defined`)
			c.Redirect(http.StatusMovedPermanently, "/")
		} // switch
	})

	return
} // setupRouter ()

// TODO simplify (gocognit 17)
//
// handleInput handles user input
func handleInput(c *gin.Context, winType string) {
	//
	// TODO: check input (general and depending on type)
	// TODO: give feedback on action?
	//
	switch winType {
	// Handle add watchlist input
	case "wadd":
		element := c.Query("addname")

		if element != "" {
			postgresql.AddWatchlist(element)
			c.Data(http.StatusOK, "text/plain", []byte(element))
		} // if
	// Handle add portfolio input
	case "padd":
		var (
			trisk, prisk = 0.0, 0.0
			err          error
		)

		if c.Query("paddtrade_risk") != "" {
			trisk, err = strconv.ParseFloat(c.Query("paddtrade_risk"), 64)
			misc.ShowError(err, "", "ErrPrint")
		} // if

		if c.Query("paddport_risk") != "" {
			prisk, err = strconv.ParseFloat(c.Query("paddport_risk"), 64)
			misc.ShowError(err, "", "ErrPrint")
		} // if

		if c.Query("paddname") != "" && trisk > 0.0 && prisk > 0.0 {
			postgresql.AddPortfolio(c.Query("paddname"), trisk, prisk)
			c.Data(http.StatusOK, "text/plain", []byte(c.Query("paddname")))
		} // if
	// Handle add symbol input
	case "sadd":
		if c.Query("saddtype") == "Warrant" {
			postgresql.AddSymbolWithWarrant(
				c.Query("saddsymbol"),
				c.Query("saddname"),
				c.Query("saddwkn"),
				c.Query("saddisin"),
				c.Query("saddsector"),
				c.Query("saddtype"),
				c.Query("saddquote_cur"),
				c.Query("saddbasesym"),
				c.Query("saddoptionend"),
			)
		} else {
			postgresql.AddSymbol(
				c.Query("saddsymbol"),
				c.Query("saddname"),
				c.Query("saddwkn"),
				c.Query("saddisin"),
				c.Query("saddsector"),
				c.Query("saddtype"),
				c.Query("saddquote_cur"),
			)
		} // if
	} // switch
} // handleInput()

func main() {
	var (
		appName    = "pf"
		configFile = appName + ".yml"
		configPath = "/etc/" + appName
		config     = configuration.ReadProgConfig(configPath + "/" + configFile)
	)

	// open database connection
	err := postgresql.NewDBConnection(
		config.String("database.user"),
		config.String("database.host"),
		config.String("database.password"),
		config.String("database.port"),
		config.String("database.dbname"),
		appName,
		config.String("database.sslmode"))
	misc.ShowError(err, "Could not open postgres database.", "ErrMsgFatal")

	router := setupRouter()
	misc.ShowError(router.Run(config.String("server.port")), "", "ErrPanic")
} // main ()
