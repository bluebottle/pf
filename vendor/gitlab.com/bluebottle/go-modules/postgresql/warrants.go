// Package postgresql - Some PostgreSQL using functions
package postgresql

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Warrant is the structure of a warrant.
type Warrant struct {
	Symbol           string
	Base_symbol      string
	Warrant_end_date time.Time
}

var warrant Warrant

// // FindAllPortfolios returns all portfolios in the portfolio table.
// func FindAllPortfolios() (pFolios []Portfolio, err error) {
// 	var conn *pgxpool.Conn

// 	if conn, err = pool.Acquire(context.Background()); err != nil {
// 		return []Portfolio{}, err
// 	} // if

// 	if rows, err = conn.Query(context.Background(), "select portfolio_id, name, current_capital, trade_risk, port_risk from public.portfolio"); err != nil {
// 		return []Portfolio{}, err
// 	} // if

// 	var tmpFolio Portfolio

// 	for rows.Next() {
// 		if tmpFolio, err = GetNextPortfolioRow(rows); err != nil {
// 			return []Portfolio{}, err
// 		} // if

// 		pFolios = append(pFolios, tmpFolio)
// 	} // for

// 	conn.Release()
// 	return
// } // FindAllPortfolios()

// FindSpecificWarrant returns warrant with given symbol
func FindSpecificWarrant(symbol string) (Warrant, error) {
	var conn *pgxpool.Conn

	if conn, err = pool.Acquire(context.Background()); err != nil {
		return Warrant{}, err
	} // if

	defer conn.Release()

	if _, err = conn.Conn().Prepare(context.Background(), "findWarr", "select symbol, base_symbol, warrant_end_date from public.warrant where symbol=$1"); err != nil {
		return Warrant{}, err
	} // if

	if rows, err = conn.Query(context.Background(), "findWarr", symbol); err != nil {
		return Warrant{}, err
	} // if

	defer rows.Close()

	if rows.Next() {
		return GetNextWarrantRow(rows)
	} else {
		return Warrant{}, nil
	} // if
} // FindSpecificWarrant()

// GetNextWarrantRow returns the next warrant row.
func GetNextWarrantRow(rows pgx.Rows) (Warrant, error) {
	err := rows.Scan(&warrant.Symbol, &warrant.Base_symbol, &warrant.Warrant_end_date)
	return warrant, err
} // GetNextWarrantRow ()

// // AddEmptyPortfolio will add a new, empty portfolio
// func AddEmptyPortfolio(name string) {
// 	AddPortfolio(name, 0.0, 0.0, 0.0)
// } // AddEmptyPortfolio ()

// // AddPortfolio will add a new portfolio with given values
// func AddPortfolio(name string, cc, tr, pr float64) {
// 	tmpPort, err := FindSpecificPortfolio(name)
// 	misc.ShowError(err, "", "ErrPanic")

// 	// add new portfolio if no portfolio with that name exists
// 	if tmpPort.Name != name {
// 		fmt.Println("Creating portfolio " + name)
// 		conn, err = pool.Acquire(context.Background())
// 		misc.ShowError(err, "", "ErrPanic")
// 		cmd := fmt.Sprintf("insert into public.portfolio(name, current_capital, trade_risk, port_risk) values ('%s', %f, %f, %f)", name, cc, tr, pr)
// 		_, err := conn.Exec(context.Background(), cmd)
// 		misc.ShowError(err, "", "ErrPanic")
// 		conn.Release()
// 	} // if
// } // AddPortfolio ()

// insertWarrant does the real insertion into the database
func insertWarrant(tx pgx.Tx, conn *pgxpool.Conn, symbol, baseSymbol, warrendEnd string) (err error) {
	cmd := fmt.Sprintf("insert into public.warrant(symbol, base_symbol, warrant_end_date) values ('%s', '%s', '%s') on conflict do nothing", symbol, baseSymbol, warrendEnd)

	if tx == nil {
		_, err = conn.Exec(context.Background(), cmd)
	} else {
		_, err = tx.Exec(context.Background(), cmd)
	} // if

	return
} // insertWarrant ()
