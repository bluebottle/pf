// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Quote is the structure of a quote.
type Quote struct {
	Symbol    string    // symbol
	QuoteDate time.Time // quote_date
	Open      float64   // open
	High      float64   // high
	Low       float64   // low
	Close     float64   // close
	Volume    int64     // volume
}

var quotes Quote

// NewQuote returns an empty quote
func NewQuote() Quote {
	return Quote{Symbol: "", QuoteDate: time.Now(), Open: 0.0, High: 0.0, Low: 0.0, Close: 0.0, Volume: 0.0}
} // NewQuote ()

// GetLastQuoteDate returns the last quote date for given symbol
func GetLastQuoteDate(symbol string) (tmpDate string) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	rows, err = conn.Query(context.Background(), "select symbol, quote_date, open, high, low, close, volume from public.quote where symbol='"+symbol+"' order by quote_date desc limit 1")
	misc.ShowError(err, "", "ErrPanic")
	defer rows.Close()

	for rows.Next() {
		quote, err := getNextQuoteRow(rows)
		misc.ShowError(err, "", "ErrPanic")
		tmpDate = quote.QuoteDate.Format(time.RFC3339)
	} // for

	conn.Release()
	return
} // GetLastQuoteDate ()

// FindQuotes selects all rows in the quotes table for a given symbol, sorted by quote_date and given sort order.
func FindQuotes(symbol, order string) (tmpQuote []Quote) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	tmpRows := findQuoteRows(symbol, order)

	for tmpRows.Next() {
		quote, err := getNextQuoteRow(tmpRows)
		misc.ShowError(err, "", "ErrPanic")
		tmpQuote = append(tmpQuote, quote)
	} // for

	conn.Release()
	return
} // FindQuotes()

// GetLastQuote returns the last quote for given symbol
func GetLastQuote(symbol string) (quote Quote) {
	quote = NewQuote()
	tmpQuote := FindQuotes(symbol, "desc")

	if len(tmpQuote) > 0 {
		quote = tmpQuote[0]
	} // if

	return
} // GetLastQuote()

// findQuoteRows selects all rows in the quotes table for a given symbol, sorted by quote_date and given sort order.
func findQuoteRows(symbol, order string) pgx.Rows {
	rows, err := conn.Query(context.Background(), "select symbol, quote_date, open, high, low, close, volume from public.quote where symbol='"+symbol+"' order by quote_date "+order)
	misc.ShowError(err, "", "ErrPanic")
	return rows
} // findQuoteRows()

// getNextQuoteRow returns the next quote row.
func getNextQuoteRow(rows pgx.Rows) (Quote, error) {
	err := rows.Scan(&quotes.Symbol, &quotes.QuoteDate, &quotes.Open, &quotes.High, &quotes.Low, &quotes.Close, &quotes.Volume)
	return quotes, err
} // getNextQuoteRow ()

// ToStringSlice returns a slice of string contaning date, open, close, high, low and volume
// Currently not used
// func (q *Quote) ToStringSlice() []string {
// 	return []string{q.QuoteDate.String(),
// 		fmt.Sprintf("%f", q.Open), fmt.Sprintf("%f", q.Close),
// 		fmt.Sprintf("%f", q.High), fmt.Sprintf("%f", q.Low),
// 		fmt.Sprintf("%d", q.Volume)}
// } // ToStringSlice ()

// WeeklyQuotes analyzes all the given daily values and returns the weekly values accordingly
// The highest and lowest prices in the five weekly trading sessions become the high and low for the weekly marker
// The open of the first day becomes the open of the week
// The close of the last day becomes the close of the week
func WeeklyQuotes(values []Quote) []Quote {
	var (
		tmpResult []Quote
		weekQuote = NewQuote()
	)

	for i, v := range values {
		// Append weekly quote, create new empty quote and set base values
		if v.QuoteDate.Weekday().String() == "Monday" || i == 0 {
			if i > 0 {
				tmpResult = append(tmpResult, weekQuote)
				weekQuote = NewQuote()
			} // if

			weekQuote = Quote{Symbol: v.Symbol, QuoteDate: v.QuoteDate, Open: v.Open}
		} // if

		// check if we have a higher weekly high
		if v.High > weekQuote.High {
			weekQuote.High = v.High
		} // if

		// check if we have a lower weekly low or it is still 0.0
		if v.Low < weekQuote.Low || weekQuote.Low == 0.0 {
			weekQuote.Low = v.Low
		} // if

		// Add low on every day to avoid short week errors
		weekQuote.Close = v.Close
		// Add volume to weekly
		weekQuote.Volume += v.Volume
	} // for

	// add the last quote if we have any
	if len(values) > 0 {
		weekQuote.Close = values[len(values)-1].Close
	} // if

	// return last week added
	return append(tmpResult, weekQuote)
} // WeeklyQuotes ()

// MonthlyQuotes analyzes all the given daily values and returns the monthly values accordingly
// The highest and lowest prices in the monthly trading sessions become the high and low for the monthly marker
// The open of the first day becomes the open of the month
// The close of the last day becomes the close of the month
func MonthlyQuotes(values []Quote) []Quote {
	var (
		tmpResult  []Quote
		monthQuote = NewQuote()
		oldMonth   = ""
	)

	for i, v := range values {
		// If the date is a new month and it is not the first value append quote to monthly and get an empty quote
		if v.QuoteDate.Month().String() != oldMonth {
			if i > 0 {
				monthQuote.Close = values[i-1].Close
				tmpResult = append(tmpResult, monthQuote)
				monthQuote = NewQuote()
			} // if

			monthQuote = Quote{Symbol: v.Symbol, QuoteDate: v.QuoteDate, Open: v.Open}
			oldMonth = v.QuoteDate.Month().String()
		} // if

		// check if we have a higher monthly high
		if v.High > monthQuote.High {
			monthQuote.High = v.High
		} // if

		// check if we have a lower monthly low or it is still 0.0
		if v.Low < monthQuote.Low || monthQuote.Low == 0.0 {
			monthQuote.Low = v.Low
		} // if

		// Add volume to monthly and store days closing value for later insert as monthly close
		monthQuote.Volume += v.Volume
	} // for

	// set the last monthly quote if we have any
	if len(values) > 0 {
		monthQuote.Close = values[len(values)-1].Close
	} // if

	// return with last week added
	return append(tmpResult, monthQuote)
} // MonthlyQuotes ()

// YearlyQuotes analyzes all the given daily values and returns the yearly values accordingly
// The highest and lowest prices in the yearly trading sessions become the high and low for the yearly marker
// The open of the first day becomes the open of the year
// The close of the last day becomes the close of the year
func YearlyQuotes(values []Quote) []Quote {
	var (
		tmpResult []Quote
		yearQuote = NewQuote()
		oldYear   int
	)

	for i, v := range values {
		// If the date is a new year and it is not the first value append quote to yearly and get an empty quote
		if v.QuoteDate.Year() != oldYear {
			if i > 0 {
				yearQuote.Close = values[i-1].Close
				tmpResult = append(tmpResult, yearQuote)
				yearQuote = NewQuote()
			} // if

			yearQuote = Quote{Symbol: v.Symbol, QuoteDate: v.QuoteDate, Open: v.Open}
			oldYear = v.QuoteDate.Year()
		} // if

		// check if we have a higher yearly high
		if v.High > yearQuote.High {
			yearQuote.High = v.High
		} // if

		// check if we have a lower yearly low or it is still 0.0
		if v.Low < yearQuote.Low || yearQuote.Low == 0.0 {
			yearQuote.Low = v.Low
		} // if

		// Add volume to yearly and store days closing value for later insert as yearly close
		yearQuote.Volume += v.Volume
	} // for

	// set the last yearly quote if we have any
	if len(values) > 0 {
		yearQuote.Close = values[len(values)-1].Close
	} // if

	// return with last values added
	return append(tmpResult, yearQuote)
} // YearlyQuotes ()

// UpsertQuotes inserts given quotes values into the database or updates open, high, low, close and volume if we already have a row
func UpsertQuotes(secu string, quotes []Quote) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	tx, err := conn.BeginTx(context.Background(), pgx.TxOptions{IsoLevel: pgx.Serializable})
	misc.ShowError(err, "", "ErrPanic")

	for _, v := range quotes {
		cmd := fmt.Sprintf("insert into public.quote values ('%s', '%s', %f, %f, %f, %f, %d) on conflict on constraint quote_pkey do update set open=excluded.open, high=excluded.high, low=excluded.low, close=excluded.close, volume=excluded.volume", v.Symbol, v.QuoteDate.Format("2006-01-02"), v.Open, v.High, v.Low, v.Close, v.Volume)
		_, err := conn.Exec(context.Background(), cmd)
		misc.ShowError(err, "", "ErrPanic")
	} // for

	err = tx.Commit(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	conn.Release()
} // UpsertQuotes ()

// GetClose extracts closing values from given quotes as slice of float64
func GetClose(quotes []Quote) (closeVal []float64) {
	for _, v := range quotes {
		closeVal = append(closeVal, v.Close)
	} // for

	return
} // GetClose()
