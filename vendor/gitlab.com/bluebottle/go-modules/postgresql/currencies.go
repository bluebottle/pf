// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"context"

	"github.com/jackc/pgx/v4"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Currencies contains the structure of a currencies sentence
type Currencies struct {
	Currency string
	Currencycountry string
	Iso_code string
	Symbol string
}

// NewCurrency returns an empty currency row
func NewCurrency() Currencies {
	return Currencies{
		Currency: "",
		Currencycountry: "",
		Iso_code: "",
		Symbol: "",
	}
} // NewLookup ()

var currencies Currencies

// GetCurrencySymbol gets the symbol for given ISO code
// If ISO code is not found an empty currency value is returned
func GetCurrencySymbol(iso string) Currencies {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	rows, err = conn.Query(context.Background(), "select currency, currencycountry, iso_code, symbol from public.currencies where iso_code='"+iso+"' limit 1")
	misc.ShowError(err, "", "ErrPanic")

	if rows.Next() {
		currencies = GetNextCurrencyRow(rows)
	} else {
		currencies = NewCurrency()
	} // if

	conn.Release()
	return currencies
} // GetCurrencySymbol()

// GetAllCurrencies gets all existing currencies
func GetAllCurrencies() (curArr []Currencies) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	rows, err = conn.Query(context.Background(), "select currency, currencycountry, iso_code, symbol from public.currencies order by iso_code")
	misc.ShowError(err, "", "ErrPanic")

	for rows.Next() {
		curArr = append (curArr, GetNextCurrencyRow(rows))
	} // for

	conn.Release()
	return
} // GetAllCurrencies()

// GetNextCurrencyRow returns the next curremcy row.
func GetNextCurrencyRow(rows pgx.Rows) Currencies {
	err := rows.Scan(&currencies.Currency, &currencies.Currencycountry, &currencies.Iso_code, &currencies.Symbol)
	misc.ShowError(err, "", "ErrPanic")
	return currencies
} // GetNextCurrencyRow ()
