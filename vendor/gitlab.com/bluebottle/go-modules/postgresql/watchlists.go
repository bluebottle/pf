// Package postgresql - Some PostgreSQL using functions
package postgresql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Watchlist is the structure of a watchlist.
type Watchlist struct {
	WatchlistID   int64
	WatchlistName string
}

var watchlist Watchlist

// FindAllWatchlists returns all watchlists.
func FindAllWatchlists() (watchs []Watchlist, err error) {
	var conn *pgxpool.Conn

	if conn, err = pool.Acquire(context.Background()); err != nil {
		return []Watchlist{}, err
	} // if

	if rows, err = conn.Query(context.Background(), "select watchlist_id, watch_name from public.watchlists order by watch_name"); err != nil {
		return []Watchlist{}, err
	} // if

	var tmpWatch Watchlist

	for rows.Next() {
		if tmpWatch, err = GetNextWatchlistRow(rows); err != nil {
			return []Watchlist{}, err
		} // if

		watchs = append(watchs, tmpWatch)
	} // for

	conn.Release()
	return
} // FindAllWatchlists()

// // FindSpecificWarrant returns warrant with given symbol
// func FindSpecificWarrant(symbol string) (Warrant, error) {
// 	var conn *pgxpool.Conn

// 	if conn, err = pool.Acquire(context.Background()); err != nil {
// 		return Warrant{}, err
// 	} // if

// 	defer conn.Release()

// 	if _, err = conn.Conn().Prepare(context.Background(), "findWarr", "select symbol, base_symbol, warrant_end_date from public.warrant where symbol=$1"); err != nil {
// 		return Warrant{}, err
// 	} // if

// 	if rows, err = conn.Query(context.Background(), "findWarr", symbol); err != nil {
// 		return Warrant{}, err
// 	} // if

// 	defer rows.Close()

// 	if rows.Next() {
// 		return GetNextWarrantRow(rows)
// 	} else {
// 		return Warrant{}, nil
// 	} // if
// } // FindSpecificWarrant()

// GetNextWatchlistRow returns the next watchlist row.
func GetNextWatchlistRow(rows pgx.Rows) (Watchlist, error) {
	err := rows.Scan(&watchlist.WatchlistID, &watchlist.WatchlistName)
	return watchlist, err
} // GetNextWatchlistRow ()

// AddWatchlist will add a new watchlist. If it already exists, nothing is done.
func AddWatchlist(name string) {
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	cmd := fmt.Sprintf("insert into public.watchlists(watch_name) values ('%s') on conflict do nothing", name)
	_, err := conn.Exec(context.Background(), cmd)
	misc.ShowError(err, "", "ErrPanic")
	conn.Release()
} // AddWatchlist ()
