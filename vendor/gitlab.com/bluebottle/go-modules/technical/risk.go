// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import "gitlab.com/bluebottle/go-modules/postgresql"

// CalculateCurrentRisk calculates the current risk
func CalculateCurrentRisk(v *postgresql.Trade) (risk float64) {
	// Possible long combinations:
	// CP BP SP   => risk is CP
	// CP SP BP   => risk is CP
	// BP CP SP   => risk is CP
	// BP SP CP   => risk is (CP - SP)
	// SP BP CP   => risk is (CP - SP)
	// SP CP BP   => risk is (CP - SP)
	//
	// TODO: Adapt to new trade structure (if done => redo test)
	//
	// switch {
	// case ((v.StopLossValue >= v.BuyPrice) && (v.CurrPrice >= v.StopLossValue)) ||
	// 	((v.StopLossValue < v.BuyPrice) && (v.CurrPrice >= v.BuyPrice)) ||
	// 	((v.StopLossValue < v.CurrPrice) && (v.CurrPrice < v.BuyPrice) && (v.StopLossValue != 0)):
	// 	risk = (v.CurrPrice - v.StopLossValue) * v.BuyQuantity
	// case v.StopLossValue == 0:
	// 	risk = v.BuyPrice * v.BuyQuantity
	// default:
	// 	risk = v.CurrPrice * v.BuyQuantity
	// } // switch

	return
} // CalculateCurrentRisk ()
