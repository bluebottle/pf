package main

import (
	"html/template"
)

// Watchlist contains the values of a watchlist
type Watchlist struct {
	Name string
}

//
// TODO: add watchlists layout (similar to portfolio?)
// TODO: add functionality to delete existing watchlist
//

func watchaddwin() (content template.HTML) {
	//
	// TODO: Add functionality to add a new watchlist (form, checks, buttons, status messages)
	// TODO: Check test error "2022/10/04 00:20:35 ERROR: relation "public.watchlists" does not exist (SQLSTATE 42P01)"
	//
	content = template.HTML(`<div id="addwatchform">
		<label for="waddname">Name:</label>
		<input name="waddname" id="waddname" type="text" size="50" maxlength="255" value="" />
	</div>
	<div id="addwatchbuttons">
		<input type="button" onclick="watchaddfunc()" value="Save">
	</div>`)
	return
} // watchaddwin()

func watchlist(portfolio string) (content template.HTML) {
	//
	// TODO: Add watchlist content
	//
	return
} // watchlist()
