// Package configuration handles the configuration functionality.
package configuration

import (
	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/confmap"
	"github.com/knadh/koanf/providers/file"

	"gitlab.com/bluebottle/go-modules/misc"
)

// ReadProgConfig Reads configuration file and returns content, with default values for entities not set.
func ReadProgConfig(configfile string) (v *koanf.Koanf) {
	v = koanf.New(".")
	//
	// TODO: Minimize the use of hardcoded values
	//
	err := v.Load(confmap.Provider(
		map[string]interface{}{
			"database.type":    "postgresql",
			"database.host":    "localhost",
			"database.port":    5432,
			"database.user":    "postgres",
			"database.pass":    "",
			"database.dbname":  "pf",
			"database.sslmode": "prefer",
			"server.port":      8000,
		}, "."), nil)
	misc.ShowError(err, "", "ErrPanic")
	err = v.Load(file.Provider(configfile), yaml.Parser())
	misc.ShowError(err, "", "ErrPanic")
	return
} // readProgConfig ()

func ReadBatchConfig(filename string) (v *koanf.Koanf) {
	v = koanf.New(".")
	err := v.Load(file.Provider(filename), yaml.Parser())
	misc.ShowError(err, "error when reading config: "+filename, "ErrMsgFatal")
	return
} // readBatchConfig ()
